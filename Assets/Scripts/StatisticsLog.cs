﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class StatisticsLog : MonoBehaviour
{
    static Dictionary<string, float> files = new Dictionary<string, float>();

    public static string BASE_PATH = "Assets/Resources/";

    public static void AddEntry(string file, float dt, params float[] values)
    {
        if (!files.ContainsKey(file))
        {
            File.Delete(BASE_PATH + file + ".csv");
            files.Add(file, 0);
        }

        float time = files[file] + dt;

        string valuesLine = time.ToString();
        foreach (float value in values)
        {
            valuesLine += "\t" + value;
        }

        valuesLine += "\n";

        File.AppendAllText(BASE_PATH + file + ".csv", valuesLine);

        files[file] = time;
    }

}
