﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Wheel", menuName = "Model Elements/Wheel Model", order = 7)]
[System.Serializable]
public class WheelModel : ScriptableObject, ElementModel
{
    public TireModel tire;

    public float radius; // [m]

    public float axleLoadCoefficient;

    public float mass;

    public float GetCumulativeMass()
    {
        return mass + tire.GetCumulativeMass();
    }
}
