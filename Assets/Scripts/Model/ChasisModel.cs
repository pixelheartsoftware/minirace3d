﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Chasis", menuName = "Model Elements/Chasis Model", order = 2)]
[System.Serializable]
public class ChasisModel : ScriptableObject, ElementModel
{
    public float mass; // kilograms
    public Vector3 dimensions; // x - width, y - length

    public float GetCumulativeMass()
    {
        return mass;
    }
}
