﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AutomaticTransmission", menuName = "Model Elements/Automatic Transmission", order = 6)]
[System.Serializable]
public class AutomaticTransmission : ScriptableObject, ElementModel
{
    public TorqueConverterModel torqueConverter;

    public GearboxModel gearbox;

    public float GetCumulativeMass()
    {
        return torqueConverter.GetCumulativeMass() + gearbox.GetCumulativeMass();
    }
}
