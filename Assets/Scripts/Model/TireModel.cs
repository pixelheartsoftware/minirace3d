﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Tire", menuName = "Model Elements/Tire Model", order = 6)]
[System.Serializable]
public class TireModel : ScriptableObject, ElementModel
{
    public float[] Fy;
    public float[] Mz;
    public float[] Fx;

    public float frictionCoefficient;

    public float mass;

    public float GetCumulativeMass()
    {
        return mass;
    }
}
