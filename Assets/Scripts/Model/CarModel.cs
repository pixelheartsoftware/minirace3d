﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Car", menuName = "Model Elements/Car Model", order = 1)]
[System.Serializable]
public class CarModel : ScriptableObject, ElementModel
{
    public ChasisModel chasis;

    public EngineModel engine;

    public GearboxModel gearbox;

    public WheelModel wheelFL, wheelFR, wheelRL, wheelRR;

    public SteeringModel steering;

    public TorqueConverterModel torqueConverter;

    public float GetCumulativeMass()
    {
        return chasis.GetCumulativeMass() +
            engine.GetCumulativeMass() +
            gearbox.GetCumulativeMass() +
            wheelFL.GetCumulativeMass() +
            wheelFR.GetCumulativeMass() +
            wheelRL.GetCumulativeMass() +
            wheelRR.GetCumulativeMass() +
            steering.GetCumulativeMass();
    }
}
