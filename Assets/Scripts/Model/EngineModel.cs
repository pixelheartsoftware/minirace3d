﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Engine", menuName = "Model Elements/Engine/Engine Model", order = 3)]
[System.Serializable]
public class EngineModel : ScriptableObject, ElementModel
{
    public float inertia; // [kg*m^2]

    public float mass;     // [kg]

    public float minSpeed, maxSpeed; // rpm

    public FloatArray[] engineMap;

    public int[] throttleIndexes;
    public int[] engineSpeedIndexes;

    public float GetCumulativeMass()
    {
        return mass;
    }

    /// <summary>
    /// Returns engine torque for the given parameters.
    /// </summary>
    /// <param name="engineRotationalSpeed">Rotational speed in radians per second.</param>
    /// <param name="throttlePosition">Percent (0 to 1 inclusive) of throttle press.</param>
    /// <returns>engine torque in Nm</returns>
    public virtual float GetTorque(float engineRotationalSpeed, float throttlePosition)
    {
        engineRotationalSpeed = Mathf.Clamp(engineRotationalSpeed, minSpeed, maxSpeed);

        float throttlePositionPercent = throttlePosition;

        float throttleIndex = GetIndex(throttlePositionPercent, throttleIndexes);
        int throttleBaseIndex = (int) throttleIndex;

        float rpmIndex = GetIndex(engineRotationalSpeed, engineSpeedIndexes);
        int rpmBaseIndex = (int)rpmIndex;

        float engineTorque;

        try
        {
            if (throttleIndex > throttleBaseIndex)
            {
                FloatArray speeds1 = engineMap[throttleBaseIndex];
                FloatArray speeds2 = engineMap[throttleBaseIndex + 1];

                float fractionThrottle = throttleIndex - throttleBaseIndex;

                if (rpmIndex > rpmBaseIndex)
                {
                    float rpmFraction = rpmIndex - rpmBaseIndex;

                    float torque1 = Mathf.Lerp(speeds1.items[rpmBaseIndex], speeds1.items[rpmBaseIndex + 1], rpmFraction);
                    float torque2 = Mathf.Lerp(speeds2.items[rpmBaseIndex], speeds2.items[rpmBaseIndex + 1], rpmFraction);

                    engineTorque = Mathf.Lerp(torque1, torque2, fractionThrottle);
                }
                else
                {
                    engineTorque = Mathf.Lerp(speeds1.items[rpmBaseIndex], speeds2.items[rpmBaseIndex], fractionThrottle);
                }
            }
            else
            {
                FloatArray speeds = engineMap[throttleBaseIndex];

                if (rpmIndex > rpmBaseIndex)
                {
                    float fraction = rpmIndex - rpmBaseIndex;

                    engineTorque = Mathf.Lerp(speeds.items[rpmBaseIndex], speeds.items[rpmBaseIndex + 1], fraction);
                }
                else
                {
                    engineTorque = speeds.items[rpmBaseIndex];
                }
            }
        } catch (Exception e)
        {
            Debug.Log("rpmBaseIndex: [" + rpmBaseIndex + "], throttleBaseIndex: [" + throttleBaseIndex + "], engineRotationalSpeed: [" + engineRotationalSpeed + "], throttle: [" + throttlePosition + "]");
            throw e;
        }

        return engineTorque;
    }

    private float GetIndex(float value, int[] indexes)
    {
        if (value < indexes[0])
        {
            return 0;
        }

        if (value >= indexes[indexes.Length - 1])
        {
            return indexes.Length - 1;
        }

        for (int i = 0; i < indexes.Length - 1; i++)
        {
            if (value >= indexes[i] && value < indexes[i + 1])
            {
                return i + ((value - indexes[i]) / (indexes[i + 1] - indexes[i]));
            }
        }

        throw new Exception("This should not happen: value: [" +value+ "] indexes: [" + indexes + "]");
    }
}

[System.Serializable]
public class FloatArray
{
    public float[] items;
}