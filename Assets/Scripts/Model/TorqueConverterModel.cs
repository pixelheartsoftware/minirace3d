﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TorqueConverter", menuName = "Model Elements/Torque Converter", order = 6)]
[System.Serializable]
public class TorqueConverterModel : ScriptableObject, ElementModel
{
    public float mass;

    public float activeDiameter; // Torque converter (impeller) active diameter [m]

    public float fluidDensity; // Torque converter fluid density [kg·m3]

    public float[] ITC; // torque converter speed ratio

    public float[] KTC; // torque converter torque coefficient

    public float[] LTC; // torque converter performance coefficient

    internal float InterpolateKTC(float index)
    {
        return Interpolate(index, KTC);
    }

    internal float InterpolateLTC(float index)
    {
        return Interpolate(index, LTC);
    }

    internal float Interpolate(float index, float[] array)
    {
        int indexRounded = (int)index;

        if (index > indexRounded)
        {
            return Mathf.Lerp(array[indexRounded], array[indexRounded + 1], index - indexRounded);
        } else
        {
            return array[indexRounded];
        }
    }

    internal float GetMatchingRatioIndex(float speedRatio)
    {
        if (speedRatio <= ITC[0])
        {
            return 0;
        }

        if (speedRatio >= ITC[ITC.Length - 1])
        {
            return ITC.Length - 1;
        }

        for (int i = 0; i < ITC.Length - 1; i++)
        {
            if (ITC[i] > speedRatio)
            {
                return i;
            } else if ((ITC[i] < speedRatio) && (ITC[i+1] > speedRatio))
            {
                return i + (ITC[i] - speedRatio) / (ITC[i + 1] - ITC[i]);
            }
        }

        throw new Exception("Bad state: speedRatio: [" + speedRatio + "], ITC: [" + ITC + "]");
    }

    public float GetCumulativeMass()
    {
        return mass;
    }
}
