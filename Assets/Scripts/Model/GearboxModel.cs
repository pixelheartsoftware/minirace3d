﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Gearbox", menuName = "Model Elements/Gearbox Model", order = 4)]
[System.Serializable]
public class GearboxModel : ScriptableObject, ElementModel
{
    public float differentialRatio;

    public float[] gearRatioMap;

    public float[] gearEfficiencyMap;

    public float mass;

    public float GetCumulativeMass()
    {
        return mass;
    }

}
