﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
    private Rigidbody myRigidbody;

    public CarModel model;

    public float throttle = 1F;

    public float brake = 0F;

    public float steeringAngle = 0F;

    public int gear = 1;

    private StateController stateController;

    // Start is called before the first frame update
    void Start()
    {
        myRigidbody = GetComponent<Rigidbody>();

        myRigidbody.mass = model.GetCumulativeMass();

        stateController = new StateController(model);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        CarStateInput input = new CarStateInput(throttle, brake, steeringAngle, gear);

        CarStateOutput output = stateController.Update(input, Time.deltaTime);

        myRigidbody.AddForce(output.force);
    }
}
