﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RearWheelState : ElementState<WheelModel, RearWheelStateInput, RearWheelStateOutput>
{
    private TireState tire = new TireState();


    public override RearWheelStateOutput GenerateOutputInternal(RearWheelStateInput input, float dt, int number)
    {
        RearWheelStateOutput output = new RearWheelStateOutput();

        output.wheelRpm = (input.vehicleSpeed / model.radius) * (30/Mathf.PI);

        return output;
    }


    public override void Init(WheelModel wheel)
    {
        base.Init(wheel);

        tire.Init(wheel.tire);

        currentState = new RearWheelStateOutput();
    }

    internal float GetRadius()
    {
        return model.radius;
    }
}

public class RearWheelStateInput : StateInput
{
    public float vehicleSpeed; // [mps]

    public RearWheelStateInput(float vehicleSpeed)
    {
        this.vehicleSpeed = vehicleSpeed;
    }
}

public class RearWheelStateOutput : StateOutput
{
    public float wheelRpm = 0; // [rpm]

}
