﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorqueConverterState : ElementState<TorqueConverterModel, TorqueConverterStateInput, TorqueConverterStateOutput>
{
    public override TorqueConverterStateOutput GenerateOutputInternal(TorqueConverterStateInput input, float dt, int number)
    {
        TorqueConverterStateOutput torqueConverterStateOutput = new TorqueConverterStateOutput();

        float speedRatio = 0;
        
        if (input.engineRpm != 0)
        {
            speedRatio = input.turbineRpm / input.engineRpm;
        }

        speedRatio = Mathf.Clamp(speedRatio, 0f, 1f);

        float i = model.GetMatchingRatioIndex(speedRatio);

        torqueConverterStateOutput.impellerTorque = model.fluidDensity * model.InterpolateLTC(i) * Mathf.Pow(input.engineRpm/60, 2) * Mathf.Pow(model.activeDiameter, 5);
        torqueConverterStateOutput.turbineTorque = model.InterpolateKTC(i) * torqueConverterStateOutput.impellerTorque;

        StatisticsLog.AddEntry("impellerTorque", dt, torqueConverterStateOutput.impellerTorque);

        StatisticsLog.AddEntry("turbineTorque", dt, torqueConverterStateOutput.turbineTorque);

        return torqueConverterStateOutput;
    }

    public override void Init(TorqueConverterModel model)
    {
        base.Init(model);
        this.currentState = new TorqueConverterStateOutput();
    }
}

public class TorqueConverterStateInput : StateInput
{
    public float turbineRpm; // [rpm]
    public float engineRpm; // [rpm]

    public TorqueConverterStateInput(float turbineSpeed, float engineSpeed)
    {
        this.turbineRpm = turbineSpeed;
        this.engineRpm = engineSpeed;
    }
}

public class TorqueConverterStateOutput : StateOutput
{
    public float impellerTorque = 100F; // [Nm]

    public float turbineTorque = 0; // [Nm]
}
