﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateController
{
    int number = 0;

    public CarState state = new CarState();

    public StateController(CarModel carModel)
    {
        state.Init(carModel);
    }

    internal CarStateOutput Update(CarStateInput input, float deltaTime)
    {
        return state.Update(input, deltaTime, number++);
    }

    internal CarStateOutput State()
    {
        return state.State();
    }
}
