﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GearboxState : ElementState<GearboxModel, GearboxStateInput, GearboxStateOutput>
{
    public override GearboxStateOutput GenerateOutputInternal(GearboxStateInput input, float dt, int number)
    {
        GearboxStateOutput output = new GearboxStateOutput();

        float gearRatio = model.gearRatioMap[input.currentGear - 1];

        output.gearboxTorque = input.turbineTorque * gearRatio * model.gearEfficiencyMap[input.currentGear - 1];
        output.turbineRpm = input.wheelRpm * model.differentialRatio * gearRatio;

        return output;
    }

    public override void Init(GearboxModel model)
    {
        base.Init(model);
        currentState = new GearboxStateOutput();
    }
}

public class GearboxStateInput : StateInput
{
    public float turbineTorque; // [Nm]

    public int currentGear; // -1, 0, 1, ... , 6

    public float wheelRpm; // [rpm]

    public GearboxStateInput(float turbineTorque, int currentGear, float wheelRpm)
    {
        this.turbineTorque = turbineTorque;
        this.currentGear = currentGear;
        this.wheelRpm = wheelRpm;
    }
}

public class GearboxStateOutput : StateOutput
{
    public float gearboxTorque = 0; // [Nm]

    public float turbineRpm = 0; // [rpm]
}