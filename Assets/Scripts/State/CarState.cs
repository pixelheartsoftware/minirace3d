﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarState : ElementState<CarModel, CarStateInput, CarStateOutput>
{
    public ChasisState chasis = new ChasisState();

    public EngineState engine = new EngineState();

    public GearboxState gearbox = new GearboxState();

    public SteeringState steering = new SteeringState();

    public TorqueConverterState torqueConverter = new TorqueConverterState();

    public FrontWheelState wheelFL = new FrontWheelState(),
                        wheelFR = new FrontWheelState();

    public RearWheelState wheelRL = new RearWheelState(), 
                        wheelRR = new RearWheelState();

    float lastSpeed = 0, currentSpeed = 0; //[mps]
    float acceleration = 0;

    public override void Init(CarModel car)
    {
        base.Init(car);

        chasis.Init(car.chasis);
        engine.Init(car.engine);
        gearbox.Init(car.gearbox);
        steering.Init(car.steering);
        torqueConverter.Init(car.torqueConverter);

        wheelFL.Init(car.wheelFL);
        wheelFR.Init(car.wheelFR);
        wheelRL.Init(car.wheelRL);
        wheelRR.Init(car.wheelRR);
    }

    public override CarStateOutput GenerateOutputInternal(CarStateInput input, float dt, int number)
    {
        EngineStateOutput engineOutput = engine.Update(new EngineStateInput(input.throttlePercent * 100, torqueConverter.State().impellerTorque), dt, number);

        TorqueConverterStateOutput torqueConverterStateOutput = torqueConverter.Update(new TorqueConverterStateInput(gearbox.State().turbineRpm, engineOutput.rotationalSpeedRpm), dt, number);

        GearboxStateOutput gearboxOutput = gearbox.Update(new GearboxStateInput(torqueConverterStateOutput.turbineTorque, input.gear, wheelRR.State().wheelRpm), dt, number);

        // gearbox force:
        float differentialGearRatio = 2.769F;
        float differentialEfficiency = 0.93F;
        float propellerShaftEfficiency = 0.994F;

        float gearboxForce = (differentialGearRatio * differentialEfficiency * propellerShaftEfficiency * gearboxOutput.gearboxTorque) / wheelRL.GetRadius();

        // traction force limit:
        float gravitationalAcceleration = 9.81F;
        float drivingWheelFrictionCoefficient = 1F;
        float drivingAxleLoadCoefficient = 0.6F;

        float tractionForceLimit = model.GetCumulativeMass() * gravitationalAcceleration * drivingWheelFrictionCoefficient * drivingAxleLoadCoefficient;

        // TRACTION
        float traction = Mathf.Min(gearboxForce, tractionForceLimit);

        // wheel resistive force:
        // inertia:
        float rotationalComponentsInertiaCoefficient = 1.1F;
        float inertia = model.GetCumulativeMass() * rotationalComponentsInertiaCoefficient * this.acceleration;

        // aerodynamic draG:
        float aeroDrag = 0;
        
        // rolling resistance:
        float roadSlope = 0;
        float roadSlopeAngle = Mathf.Atan(roadSlope/100);

        float rollingResistanceC0 = 1.3295F / 100F;
        float rollingResistanceLinearC1 = -2.8664F / 100000F;
        float rollingResistanceQuadraticC2 = 1.8036F / 10000000F;

        float rollingResistanceCoefficient = rollingResistanceC0 + rollingResistanceLinearC1 * currentSpeed + rollingResistanceQuadraticC2 * Mathf.Pow(currentSpeed, 2);

        float rollingResistanceForce = model.GetCumulativeMass() * gravitationalAcceleration * Mathf.Cos(roadSlopeAngle) * rollingResistanceCoefficient;
        
        // road slope force:
        float roadSlopeForce = model.GetCumulativeMass() * gravitationalAcceleration * Mathf.Sin(roadSlopeAngle);
        
        // breaking force:
        float breakingForce = 100 / (((input.brakePercent * 100) + 1));

        // total drag force:
        float totalDragForce = aeroDrag + rollingResistanceForce + roadSlopeForce + breakingForce;

        // net force:
        float netForce = traction - totalDragForce;

        StatisticsLog.AddEntry("netForce", dt, netForce);

        // new acceleration:
        float acceleration = netForce / (model.GetCumulativeMass() * rotationalComponentsInertiaCoefficient);

        CalculateSpeed(acceleration, dt);

        StatisticsLog.AddEntry("currentSpeed", dt, currentSpeed);

        wheelRL.Update(new RearWheelStateInput(currentSpeed), dt, number);
        wheelRR.Update(new RearWheelStateInput(currentSpeed), dt, number);

        Vector3 totalForce = new Vector3(0F, 0F, netForce);
        float totalTorque = 0F;

        SteeringStateOutput steeringOutput = steering.Update(new SteeringStateInput(chasis.GetDimensions(), 0F), dt, number);

        Debug.Log("Input: " + input + " output force: " + netForce);

        return new CarStateOutput(totalForce, totalTorque, acceleration, steeringOutput);
    }

    private void CalculateSpeed(float acceleration, float dt)
    {
        if (dt == 0 || acceleration == 0)
        {
            return;
        }
        float tmpLastSpeed = lastSpeed;
        lastSpeed = currentSpeed;
        currentSpeed = tmpLastSpeed + (acceleration * dt);

        this.acceleration = acceleration;
    }
}

public class CarStateInput : StateInput
{
    public float throttlePercent; //0..1
    public float brakePercent; //0..1

    public float steeringAngle; // degrees

    public int gear; // gear number, -1 .. 5, where 0 = idle, -1 = reverse

    public CarStateInput(float throttlePercent, float brakePercent, float steeringAngle, int gear)
    {
        this.throttlePercent = throttlePercent;
        this.brakePercent = brakePercent;
        this.steeringAngle = steeringAngle;
        this.gear = gear;
    }

    public override string ToString()
    {
        return "CarStateInput: (throttlePercent: " + throttlePercent + ", )";
    }
}

public class CarStateOutput : StateOutput
{
    public Vector3 force;
    public float torque;
    public float acceleration;

    public SteeringStateOutput steeringOutput;

    public CarStateOutput(Vector3 force, float torque, float acceleration, SteeringStateOutput steeringOutput)
    {
        this.force = force;
        this.torque = torque;
        this.acceleration = acceleration;
        this.steeringOutput = steeringOutput;
    }
}