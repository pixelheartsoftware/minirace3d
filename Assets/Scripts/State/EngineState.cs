﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class EngineState : ElementState<EngineModel, EngineStateInput, EngineStateOutput>
{
    private const float DGPS_TO_RPM = (30F / Mathf.PI);
    private const float RPM_TO_DGPS = (Mathf.PI / 30F);

    private Integrator integrator;

    private TorqueConverterState torqueConverter;

    public override EngineStateOutput GenerateOutputInternal(EngineStateInput input, float dt, int number)
    {
        EngineStateOutput engineStateOutput = new EngineStateOutput();

        float lastRotationalSpeed = State().rotationalSpeedRpm;

        float engineTorque = model.GetTorque(lastRotationalSpeed, input.throttlePercent);

        float newEngineAngularSpeed = (engineTorque - input.neighborElementTorque) * (1 / model.inertia); // in degrees per second

        float integral = integrator.Step(dt, newEngineAngularSpeed);

        StatisticsLog.AddEntry("engineIntegral", dt, newEngineAngularSpeed, integral);

        engineStateOutput.rotationalSpeedRpm = integral * DGPS_TO_RPM;

        StatisticsLog.AddEntry("engineRotationalSpeed", dt, engineStateOutput.rotationalSpeedRpm, engineTorque);

        return engineStateOutput;
    }

    public override void Init(EngineModel model)
    {
        base.Init(model);
        currentState = new EngineStateOutput();

        integrator = new Integrator(model.minSpeed * RPM_TO_DGPS, model.minSpeed * RPM_TO_DGPS, model.maxSpeed * RPM_TO_DGPS);
    }
}

public class EngineStateInput : StateInput
{
    public float throttlePercent; // [0 .. 1]

    public float neighborElementTorque; // like impeller torque or clutch torque - Nm

    public EngineStateInput(float throttlePercent, float neighborElementTorque)
    {
        this.throttlePercent = throttlePercent;
        this.neighborElementTorque = neighborElementTorque;
    }
}

public class EngineStateOutput : StateOutput
{
    public float rotationalSpeedRpm = 1000; // rpm
}

public class Integrator
{
    private float time;
    private float initialCondition;
    private float minValue;
    private float maxValue;

    private float currentValue;
    private float currentIntegral;

    public Integrator(float initialCondition, float minValue, float maxValue)
    {
        this.initialCondition = initialCondition;
        this.minValue = minValue;
        this.maxValue = maxValue;

        currentValue = initialCondition;
        currentIntegral = 0;
    }

    public float Step(float dt, float value)
    {
        currentIntegral += IntegrateSimple(currentValue, value, dt);

        currentIntegral = Mathf.Clamp(currentIntegral, minValue, maxValue);

        currentValue = value;

        return currentIntegral;
    }

    private float IntegrateSimple(float v1, float v2, float dt)
    {
        return (v1 + v2) * dt * 0.5F;
    }

}