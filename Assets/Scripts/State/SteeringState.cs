﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringState : ElementState<SteeringModel, SteeringStateInput, SteeringStateOutput>
{
    public override SteeringStateOutput GenerateOutputInternal(SteeringStateInput input, float dt, int number)
    {
        Vector3 angles = MathUtil.Ackerman(input.chasisDimensions.x, input.chasisDimensions.z, input.steeringAngle);

        return new SteeringStateOutput(-angles.x, -angles.y);
    }

}

public class SteeringStateInput : StateInput
{
    public Vector3 chasisDimensions;
    public float steeringAngle;

    public SteeringStateInput(Vector3 chasisDimensions, float steeringAngle)
    {
        this.chasisDimensions = chasisDimensions;
        this.steeringAngle = steeringAngle;
    }
}

public class SteeringStateOutput : StateOutput
{
    public float leftAngle, rightAngle;

    public SteeringStateOutput(float leftAngle, float rightAngle)
    {
        this.rightAngle = rightAngle;
        this.leftAngle = leftAngle;
    }
}