﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ElementState<M, I, O> where I : StateInput where M : ElementModel where O : StateOutput
{
    protected M model;

    protected O currentState;

    private int number = -1;

    public abstract O GenerateOutputInternal(I input, float dt, int number);

    public O Update(I input, float dt, int number)
    {
        if (number == this.number)
        {
            return currentState;
        }

        this.number = number;

        return currentState = GenerateOutputInternal(input, dt, number);
    }

    public virtual void Init(M model)
    {
        this.model = model;
    }

    public O State()
    {
        return currentState;
    }
}

public abstract class StateInput
{
}

public abstract class StateOutput
{

}