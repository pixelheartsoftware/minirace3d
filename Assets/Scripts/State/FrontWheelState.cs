﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrontWheelState : ElementState<WheelModel, FrontWheelStateInput, FrontWheelStateOutput>
{
    private TireState tire = new TireState();

    public override FrontWheelStateOutput GenerateOutputInternal(FrontWheelStateInput input, float dt, int number)
    {
        FrontWheelStateOutput output = new FrontWheelStateOutput();

        output.angularVelocity = input.forwardVelocity.magnitude / model.radius;

        return output;
    }

    internal float GetRotationalSpeed()
    {
        throw new NotImplementedException();
    }

    public override void Init(WheelModel wheel)
    {
        base.Init(wheel);

        tire.Init(wheel.tire);
    }

}

public class FrontWheelStateInput : StateInput
{
    public Vector3 forwardVelocity;

    public FrontWheelStateInput(Vector3 forwardVelocity)
    {
        this.forwardVelocity = forwardVelocity;
    }
}

public class FrontWheelStateOutput : StateOutput
{
    public float angularVelocity; // radians per second
}
