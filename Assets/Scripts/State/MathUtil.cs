﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MathUtil : MonoBehaviour
{

    public const float GRAVITY = 9.3F;

    public static Vector2 Ackerman(float w, float l, float centerAngle)
    {
        float sinCenter = Mathf.Sin(centerAngle * Mathf.Deg2Rad);
        float numerator = 2 * l * sinCenter;
        float denominatorPart1 = 2 * l * Mathf.Cos(centerAngle * Mathf.Deg2Rad);
        float denominatorPart2 = w * sinCenter;

        float denominatorLeft = denominatorPart1 + denominatorPart2;
        float denominatorRight = denominatorPart1 - denominatorPart2;

        return new Vector2(Mathf.Rad2Deg * Mathf.Atan(numerator / denominatorLeft), Mathf.Rad2Deg * Mathf.Atan(numerator / denominatorRight));
    }

    public static float AngleAboutY(Transform obj)
    {
        return obj.localEulerAngles.z;
    }

    public static float AckermanSteeringAngle(float w, float l, float wheelAngle)
    {
        return Mathf.Atan(Mathf.Tan(wheelAngle * Mathf.Deg2Rad) - 2*l / w) * Mathf.Rad2Deg;
    }

    public static float ForwardKinematicAngle(float s, float l, float centerAngle)
    {
        float res = (s / l) * Mathf.Tan(centerAngle);
        //Debug.Log("Calculating for s=" + s + ", l=" + ", angle=" + centerAngle + ", result=" + res);
        return res;
    }

    public static float AngularAccelerationWithTorque(float wheelRadius, float wheelMass, float torque)
    {
        float momentOfInetria = wheelMass * wheelRadius * wheelRadius;

        return torque / momentOfInetria;
    }

    public static float WheelForce(float axleTorque, float wheelRadius)
    {
        return axleTorque / wheelRadius;
    }

    /// <summary>
    /// Calculates tire forces using Pacejka's Magic Formula.
    /// </summary>
    /// <param name="slipAngle">Angular difference between tire's physical direction and it's velocity, in degrees.</param>
    /// <param name="slipRatio">The ratio between spin velocity and wheel velocity.</param>
    /// <param name="camber">Wheel's camber, in degrees.</param>
    /// <param name="load">The force pressing down on the wheel.</param>
    /// <returns></returns>
    public static Pacejka CalculatePacejka(float slipAngle, float slipRatio, float camber, float load, TireModel model)
    {
        camber = 0; // TODO: ignored for a while...

        load = load / 1000; // The load is measured in kN (1000 Newtons)

        float lateral = PacejkaLateralForce(slipAngle, slipRatio, camber, load, model);
        float aligning = PacejkaAligningMoment(slipAngle, slipRatio, camber, load, model);
        float longitudinal = PacejkaLongitudinal(slipAngle, slipRatio, camber, load, model);

        return new Pacejka(lateral, aligning, longitudinal);
    }

    private static float PacejkaLongitudinal(float slipAngle, float slipRatio, float camber, float load, TireModel model)
    {
        float a1 = model.Fx[0];
        float a2 = model.Fx[1];
        float a3 = model.Fx[2];
        float a4 = model.Fx[3];
        float a5 = model.Fx[4];
        float a6 = model.Fx[5];
        float a7 = model.Fx[6];
        float a8 = model.Fx[7];

        float percentSlip = slipRatio * 100F;

        float loadSquared = Mathf.Pow(load, 2);
        float C = 1.65F;
        float D = a1 * loadSquared + a2 * load;
        float BCD = (a3 * loadSquared + a4 * load) / Mathf.Exp(a5 * load);
        float B = BCD / (C * D);
        float E = a6 * loadSquared + a7 * load + a8;

        float phi = (1 - E) * percentSlip + (E / B) * Mathf.Rad2Deg * Mathf.Atan(B * percentSlip);
        float Fx = D * Mathf.Rad2Deg * Mathf.Sin(C * Mathf.Atan(B * phi));

        return Fx;
    }

    private static float PacejkaAligningMoment(float slipAngle, float slipRatio, float camber, float load, TireModel model)
    {
        float a1 = model.Mz[0];
        float a2 = model.Mz[1];
        float a3 = model.Mz[2];
        float a4 = model.Mz[3];
        float a5 = model.Mz[4];
        float a6 = model.Mz[5];
        float a7 = model.Mz[6];
        float a8 = model.Mz[7];

        float loadSquared = Mathf.Pow(load, 2);
        float C = 2.40F;
        float D = a1 * loadSquared + a2 * load;
        float BCD = (a3 * loadSquared + a4 * load) / Mathf.Exp(a5 * load);
        float B = BCD / (C * D);
        float E = a6 * loadSquared + a7 * load + a8;

        // camber effects:
        float Sh = 0;
        float Sv = 0;

        float phi = (1 - E) * (slipAngle + Sh) + (E / B) * Mathf.Rad2Deg * Mathf.Atan(B*(slipAngle + Sh)); // in radians
        float Mz = D * Mathf.Rad2Deg * Mathf.Sin(C * Mathf.Atan(B * phi)) + Sv;

        return Mz;
    }

    private static float PacejkaLateralForce(float slipAngle, float slipRatio, float camber, float load, TireModel model)
    {
        float a1 = model.Fy[0];
        float a2 = model.Fy[1];
        float a3 = model.Fy[2];
        float a4 = model.Fy[3];
        float a5 = model.Fy[4];
        float a6 = model.Fy[5];
        float a7 = model.Fy[6];
        float a8 = model.Fy[7];

        float loadSquared = Mathf.Pow(load, 2);
        float C = 1.30F;
        float D = a1 * loadSquared + a2 * load;
        float BCD = a3 * Mathf.Sin(a4 * Mathf.Atan(a5 * load));
        float B = BCD / (C*D);

        float E = a6 * loadSquared + a7 * load + a8;

        // camber effects:
        float Sh = 0;
        float Sv = 0;

        float phi = (1 - E)*(slipAngle + Sh) + (E / B) * Mathf.Rad2Deg * Mathf.Atan(B * (slipAngle + Sh)); // in degrees

        float Fy = D * Mathf.Sin(C * Mathf.Deg2Rad * Mathf.Atan(B * phi)) + Sv;

        return Fy;
    }
}

public struct Pacejka
{
    public Vector2 lateralForce;
    public float aligningMoment;
    public Vector2 longitudinalForce;

    public Pacejka(float lateral, float aligning, float longitudinal) : this()
    {
        lateralForce = new Vector2(lateral, 0f / 10);
        longitudinalForce = new Vector2(0f, longitudinal / 10);
        aligningMoment = aligning / 10;
    }

    public override String ToString()
    {
        return "(" + lateralForce + ", " + aligningMoment + ", " + longitudinalForce + ")";
    }
}
