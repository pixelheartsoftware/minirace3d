﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TireState : ElementState<TireModel, TireStateInput, TireStateOutput>
{
    public float temperature;

    public override TireStateOutput GenerateOutputInternal(TireStateInput input, float dt, int number)
    {
        throw new System.NotImplementedException();
    }

}

public class TireStateInput : StateInput
{
    public TireStateInput(float deltaTime)
    {

    }
}

public class TireStateOutput : StateOutput
{

}