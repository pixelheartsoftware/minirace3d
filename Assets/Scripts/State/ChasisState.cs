﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChasisState : ElementState<ChasisModel, ChasisStateInput, ChasisStateOutput>
{
    public override ChasisStateOutput GenerateOutputInternal(ChasisStateInput input, float dt, int number)
    {
        throw new System.NotImplementedException();
    }

    internal Vector3 GetDimensions()
    {
        return model.dimensions;
    }
}

public class ChasisStateInput : StateInput
{
    public ChasisStateInput()
    {

    }
}

public class ChasisStateOutput : StateOutput
{

}